package com.takeapps.ilparcodelabirra.util;

/**
 * Created by Nicolas Dubiansky on 16/02/19.
 */
public interface ResultListener<T> {
    public void finish(T t);
}
