package com.takeapps.ilparcodelabirra.util;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.takeapps.ilparcodelabirra.R;

import okhttp3.Request;

/**
 * Created by Nicolas Dubiansky on 21/08/18.
 */
public class SnackbarHelper {

    private static final String GET_METHOD = "GET";

    public static void showNoConnectionSnackBar(Context context, View view) {
        showNoConnectionSnackBar(context, view, R.string.no_connection_detected);
    }

    public static void showNoConnectionSnackBar(Context context, View view, int resMessage) {
        if(context == null){
            return;
        }
        hideKeyboard(context, view);
        Snackbar snackbar = Snackbar.make(view, resMessage, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.connect, view1 -> {
            Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
            context.startActivity(intent);
        });
        snackbar.show();
    }


    public static void showSimpleSnackBar(Context context, View view, int resId) {
        hideKeyboard(context, view);
        Snackbar.make(view, resId, Snackbar.LENGTH_LONG).show();
    }

    public static void showSimpleSnackBar(Context context, View view, String text) {
        hideKeyboard(context, view);
        Snackbar.make(view, text, Snackbar.LENGTH_LONG).show();
    }

    public static void showSuccessfullySnackbar(Context context, View view, String text){
      hideKeyboard(context, view);
      Snackbar snack = Snackbar.make(view, text, Snackbar.LENGTH_LONG);
      View snackView = snack.getView();
      snackView.setBackgroundColor(context.getResources().getColor(R.color.toasty_success));
      snack.show();
    }

  public static void showErrorSnackbar(Context context, View view, int resId){
    hideKeyboard(context, view);
    Snackbar snack = Snackbar.make(view, resId, Snackbar.LENGTH_LONG);
    View snackView = snack.getView();
    snackView.setBackgroundColor(context.getResources().getColor(R.color.toasty_error));
    snack.show();
  }

    public static void hideKeyboard(Context context, View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showFailureSnackbarForRequest(Context context, View view, String text, Request request){
        if(request.method().equals(GET_METHOD)){
            hideKeyboard(context, view);
            Snackbar snack = Snackbar.make(view, text, Snackbar.LENGTH_LONG);
            View snackView = snack.getView();
            snackView.setBackgroundColor(context.getResources().getColor(R.color.toasty_error));
            snack.show();
        }
    }

}
