package com.takeapps.ilparcodelabirra.util;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Nicolas Dubiansky on 17/02/19.
 */
public class SimpleNetworkCallback<T> implements Callback<T> {

    private ResultListener<T> resultListener;

    public SimpleNetworkCallback(ResultListener<T> resultListener) {
        this.resultListener = resultListener;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if(response.isSuccessful()){
            resultListener.finish(response.body());
        }else{
            resultListener.finish(null);
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        resultListener.finish(null);
    }
}
