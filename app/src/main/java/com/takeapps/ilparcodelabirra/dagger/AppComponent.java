package com.takeapps.ilparcodelabirra.dagger;

import com.takeapps.ilparcodelabirra.view.CreateClientActivity;
import com.takeapps.ilparcodelabirra.view.CreatePurchaseActivity;
import com.takeapps.ilparcodelabirra.network_controller.IlParcoRequestController;
import com.takeapps.ilparcodelabirra.view.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(IlParcoRequestController target);
    void inject(CreateClientActivity target);
    void inject(CreatePurchaseActivity target);
    void inject(MainActivity target);
}