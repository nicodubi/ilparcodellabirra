package com.takeapps.ilparcodelabirra.dagger;

import com.takeapps.ilparcodelabirra.BuildConfig;
import com.takeapps.ilparcodelabirra.network_controller.IlParcoRequestController;
import com.takeapps.ilparcodelabirra.network_controller.IlParcoServicesEndpoints;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

/**
 * Created by Nicolas Dubiansky on 14/01/19.
 */
@Module
public class AppModule {
    private static final String TAG = AppModule.class.getSimpleName();

    @Provides
    @Singleton
    public OkHttpClient provideLoggingCapableHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY :
                HttpLoggingInterceptor.Level.NONE);

        Interceptor failureAuthInterceptor = chain -> {
            Request request = chain.request();
            Response response = chain.proceed(request);
            Timber.tag(TAG).d("request %s ", request);
            Timber.tag(TAG).d("response %s ", response.toString());
            return response;
        };

        return new OkHttpClient.Builder()
                .addInterceptor(logging).addInterceptor(failureAuthInterceptor)
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    Request.Builder request = original.newBuilder();
                    request.addHeader("Content-Type", "application/json");
                    request.addHeader("Accept", "application/json");
                    return chain.proceed(request.build());
                })
                .build();
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl("https://ilparcodellabirra.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    public static IlParcoServicesEndpoints provideIlParcoServices(Retrofit retrofit) {
        return retrofit.create(IlParcoServicesEndpoints.class);
    }

    @Provides
    public IlParcoRequestController provideIlParcoRequestController(){
        return new IlParcoRequestController();
    }

}
