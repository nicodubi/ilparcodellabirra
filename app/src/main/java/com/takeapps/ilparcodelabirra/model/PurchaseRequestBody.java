package com.takeapps.ilparcodelabirra.model;

/**
 * Created by Nicolas Dubiansky on 16/02/19.
 */
public class PurchaseRequestBody {
    private String total;
    private Integer client_id;
    private String api_token;

    public PurchaseRequestBody(String total, Integer client_id) {
        this.total = total;
        this.client_id = client_id;
    }

    public String getTotal() {
        return total;
    }

    public Integer getClient_id() {
        return client_id;
    }

    public String getApi_token() {
        return api_token;
    }

    public void setApi_token(String api_token) {
        this.api_token = api_token;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public void setClient_id(Integer client_id) {
        this.client_id = client_id;
    }
}
