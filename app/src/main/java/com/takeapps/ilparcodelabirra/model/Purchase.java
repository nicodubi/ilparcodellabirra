package com.takeapps.ilparcodelabirra.model;

/**
 * Created by Nicolas Dubiansky on 16/02/19.
 */
public class Purchase {
    private String total;
    private Float points;
    private Integer client_id;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public Float getPoints() {
        return points;
    }

    public void setPoints(Float points) {
        this.points = points;
    }

    public Integer getClient_id() {
        return client_id;
    }

    public void setClient_id(Integer client_id) {
        this.client_id = client_id;
    }
}
