package com.takeapps.ilparcodelabirra.model;

/**
 * Created by Nicolas Dubiansky on 18/02/19.
 */
public class GetClientRequestBody {
    private String api_token;

    public GetClientRequestBody(String api_token) {
        this.api_token = api_token;
    }

    public String getApi_token() {
        return api_token;
    }

    public void setApi_token(String api_token) {
        this.api_token = api_token;
    }
}
