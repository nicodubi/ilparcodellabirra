package com.takeapps.ilparcodelabirra.model;

/**
 * Created by Nicolas Dubiansky on 16/02/19.
 */
public class CreateClientRequestBody {
    private String name;
    private String email;
    private String phone;
    private String nickname;
    private String skill;
    private String birthdate;
    private Integer has_pet;
    private String qr_id;
    private String api_token;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public Integer getHas_pet() {
        return has_pet;
    }

    public void setHas_pet(Integer has_pet) {
        this.has_pet = has_pet;
    }

    public String getQr_id() {
        return qr_id;
    }

    public void setQr_id(String qr_id) {
        this.qr_id = qr_id;
    }

    public String getApi_token() {
        return api_token;
    }

    public void setApi_token(String api_token) {
        this.api_token = api_token;
    }
}
