package com.takeapps.ilparcodelabirra;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.rohitss.uceh.UCEHandler;
import com.takeapps.ilparcodelabirra.dagger.AppComponent;
import com.takeapps.ilparcodelabirra.dagger.AppModule;
import com.takeapps.ilparcodelabirra.dagger.DaggerAppComponent;

import es.dmoral.toasty.Toasty;
import pl.tajchert.nammu.Nammu;
import timber.log.Timber;

/**
 * Created by Nicolas Dubiansky on 16/02/19.
 */
public class IlParcoApplication extends Application {

    protected static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Nammu.init(this);

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        createAppComponent();
        initToasty();
        setupUCEHandlerExceptionsLog();
    }

    private void setupUCEHandlerExceptionsLog() {
        if (BuildConfig.BUILD_TYPE.equals("debug")) {
            new UCEHandler.Builder(this).build();
        }
    }


    private void initToasty() {
        Toasty.Config.getInstance()
                .setErrorColor(getResources().getColor(R.color.toasty_error))
                .setInfoColor(getResources().getColor(R.color.toasty_info))
                .setSuccessColor(getResources().getColor(R.color.toasty_success))
                .setWarningColor(getResources().getColor(R.color.toasty_warning))
                .setTextColor(getResources().getColor(R.color.white))
                .tintIcon(true).setTextSize(20)
                .apply();
    }

    private static void createAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule())
                .build();
    }

    public static AppComponent getAppComponent() {
        if (appComponent == null) {
            createAppComponent();
        }
        return appComponent;
    }

    public static boolean isNetworkAvailable(Context context) {
        if (context != null) {
            ConnectivityManager conectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if ((conectivityManager != null)) {
                NetworkInfo info = conectivityManager.getActiveNetworkInfo();
                return info != null && info.isConnectedOrConnecting();
            }
        }
        return false;
    }
}
