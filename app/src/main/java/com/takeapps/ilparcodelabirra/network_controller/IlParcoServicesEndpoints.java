package com.takeapps.ilparcodelabirra.network_controller;

import com.takeapps.ilparcodelabirra.model.Client;
import com.takeapps.ilparcodelabirra.model.CreateClientRequestBody;
import com.takeapps.ilparcodelabirra.model.Purchase;
import com.takeapps.ilparcodelabirra.model.PurchaseRequestBody;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Nicolas Dubiansky on 16/02/19.
 */
public interface IlParcoServicesEndpoints {

    @POST("clients")
    Call<Client> createClient(@Body CreateClientRequestBody clientRequestBody);

    @POST("purchases")
    Call<Purchase> createPurchase(@Body PurchaseRequestBody purchaseRequestBody);

    @GET("qrs/{qrCodeId}")
    Call<Client> getClientFromQrCodeId(@Path("qrCodeId") String qrCodeId, @Query("api_token") String apiToken);
}
