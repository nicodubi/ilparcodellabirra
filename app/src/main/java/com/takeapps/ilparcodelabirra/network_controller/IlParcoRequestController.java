package com.takeapps.ilparcodelabirra.network_controller;

import com.takeapps.ilparcodelabirra.IlParcoApplication;
import com.takeapps.ilparcodelabirra.model.Client;
import com.takeapps.ilparcodelabirra.model.CreateClientRequestBody;
import com.takeapps.ilparcodelabirra.model.Purchase;
import com.takeapps.ilparcodelabirra.model.PurchaseRequestBody;
import com.takeapps.ilparcodelabirra.util.ResultListener;
import com.takeapps.ilparcodelabirra.util.SimpleNetworkCallback;

import javax.inject.Inject;

/**
 * Created by Nicolas Dubiansky on 16/02/19.
 */
public class IlParcoRequestController {
    private static final String API_TOKEN = "isiJ3BumYpNMGraxyzBiX9AaqmoSUMuWeLHCYVrYiiGlfbxmQ2VnwaNYkqg1";

    @Inject
    public IlParcoServicesEndpoints servicesEndpoints;

    public IlParcoRequestController() {
        IlParcoApplication.getAppComponent().inject(this);
    }

    public void createClient(CreateClientRequestBody clientBodyRequest, ResultListener<Client> resultListener) {
        clientBodyRequest.setApi_token(API_TOKEN);
        servicesEndpoints.createClient(clientBodyRequest).enqueue(new SimpleNetworkCallback<>(resultListener));
    }

    public void getClientInfo(String qrCodeId, ResultListener<Client> resultListener) {
        servicesEndpoints.getClientFromQrCodeId(qrCodeId, API_TOKEN).enqueue(new SimpleNetworkCallback<>(resultListener));
    }

    public void createPurchase(PurchaseRequestBody purchaseRequestBody, ResultListener<Purchase> resultListener) {
        purchaseRequestBody.setApi_token(API_TOKEN);
        servicesEndpoints.createPurchase(purchaseRequestBody).enqueue(new SimpleNetworkCallback<>(resultListener));
    }
}
