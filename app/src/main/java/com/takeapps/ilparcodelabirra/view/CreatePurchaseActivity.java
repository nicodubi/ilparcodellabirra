package com.takeapps.ilparcodelabirra.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.takeapps.ilparcodelabirra.model.Client;
import com.takeapps.ilparcodelabirra.IlParcoApplication;
import com.takeapps.ilparcodelabirra.network_controller.IlParcoRequestController;
import com.takeapps.ilparcodelabirra.model.PurchaseRequestBody;
import com.takeapps.ilparcodelabirra.R;
import com.takeapps.ilparcodelabirra.util.SnackbarHelper;

import javax.inject.Inject;

import es.dmoral.toasty.Toasty;

public class CreatePurchaseActivity extends AppCompatActivity {
    public static final String QR_CODE_ID_KEY = "QR_CODE_ID_KEY";

    private String qrCodeId;
    private Client client;
    @Inject
    public IlParcoRequestController ilParcoRequestController;
    private TextView clientName;
    private TextView clientEmail;
    private TextView clientBornDate;
    private TextView clientPoints;
    private TextView clientNickName;
    private EditText pointsEditText;
    private FloatingActionButton doneButton;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_purchase);
        setTitle(R.string.new_purchase_title);
        IlParcoApplication.getAppComponent().inject(this);
        qrCodeId = getIntent().getStringExtra(QR_CODE_ID_KEY);
        clientName = findViewById(R.id.client_name_id);
        clientEmail = findViewById(R.id.client_email_id);
        clientBornDate = findViewById(R.id.client_bornDate_id);
        clientPoints = findViewById(R.id.client_points_id);
        clientNickName = findViewById(R.id.client_nickname_id);
        pointsEditText = findViewById(R.id.points_field_edit_text_id);
        doneButton = findViewById(R.id.done_button_id);

        doneButton.setOnClickListener(v -> {
            doneButtonClick();
        });
        initProgressDialog();
        getClientInfo();


    }

    private void initProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getText(R.string.getting_client_please_wait));
    }

    private void getClientInfo() {
        if (!IlParcoApplication.isNetworkAvailable(getApplicationContext())) {
            SnackbarHelper.showNoConnectionSnackBar(this, doneButton, R.string.internet_connection_needed_to_get_client_info);
            return;
        }
        progressDialog.show();
        ilParcoRequestController.getClientInfo(qrCodeId, client -> {
            progressDialog.dismiss();
            if (client == null) {
                Toasty.error(CreatePurchaseActivity.this, R.string.error_getting_client, Toast.LENGTH_SHORT, true).show();
            } else {
                this.client = client;
                initClientInformation();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (client == null) {
            getClientInfo();
        }
    }

    private void initClientInformation() {
        clientName.setText(client.getName());
        clientEmail.setText(client.getEmail());
        clientNickName.setText(client.getNickname());
        clientPoints.setText(String.valueOf(client.getPoints()));
        clientBornDate.setText(client.getBirthdate());
    }

    private void doneButtonClick() {
        if (pointsEditText.getText() == null || TextUtils.isEmpty(pointsEditText.getText().toString())) {
            Toasty.error(CreatePurchaseActivity.this, R.string.must_input_points, Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (!IlParcoApplication.isNetworkAvailable(getApplicationContext())) {
            SnackbarHelper.showNoConnectionSnackBar(this, doneButton);
            return;
        }

        if (client == null) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this).setTitle(R.string.get_client_dialog_title)
                    .setMessage(R.string.get_client_dialog_message).setCancelable(false)
                    .setPositiveButton(R.string.get_client_info_positive_button, (dialog, which) -> {
                        getClientInfo();
                        dialog.dismiss();
                    });
            alertDialog.create().show();
            return;
        }


        Float pointsToLoad = Float.valueOf(pointsEditText.getText().toString());
        PurchaseRequestBody purchaseRequestBody = new PurchaseRequestBody(pointsEditText.getText().toString(), client.getId());
        ilParcoRequestController.createPurchase(purchaseRequestBody, purchase -> {
            if (purchase == null) {
                Toasty.error(CreatePurchaseActivity.this, R.string.error_creating_purchase, Toast.LENGTH_SHORT, true).show();
            } else {
                Toasty.success(CreatePurchaseActivity.this, R.string.creating_purchase_successful, Toast.LENGTH_SHORT, true).show();
                Intent intent = new Intent(CreatePurchaseActivity.this, MainActivity.class);
                intent.putExtra(MainActivity.SUCCES_OPERATION,true);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }
}
