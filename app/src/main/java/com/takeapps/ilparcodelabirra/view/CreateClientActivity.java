package com.takeapps.ilparcodelabirra.view;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.widget.Switch;
import android.widget.Toast;

import com.takeapps.ilparcodelabirra.model.CreateClientRequestBody;
import com.takeapps.ilparcodelabirra.IlParcoApplication;
import com.takeapps.ilparcodelabirra.network_controller.IlParcoRequestController;
import com.takeapps.ilparcodelabirra.R;
import com.takeapps.ilparcodelabirra.util.SnackbarHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import javax.inject.Inject;

import es.dmoral.toasty.Toasty;

public class CreateClientActivity extends AppCompatActivity {

    public static final String QR_CODE_ID_KEY = "QR_CODE_ID_KEY";
    private static final Integer HAS_PET_TRUE = 1;
    private static final Integer HAS_PET_FALSE = 0;

    private String qrCodeId;
    private TextInputEditText nameField;
    private TextInputEditText emailField;
    private TextInputEditText phoneField;
    private TextInputEditText nickNameField;
    private TextInputEditText abilityField;
    private TextInputEditText bornDateField;
    private Switch hasPetSwitch;
    private FloatingActionButton doneButton;
    @Inject
    public IlParcoRequestController ilParcoRequestController;
    private ProgressDialog progressDialog;
    private DatePickerDialog datePickerDialog;
    private Calendar dateSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_client);
        setTitle(R.string.new_client_title);
        IlParcoApplication.getAppComponent().inject(this);
        qrCodeId = getIntent().getStringExtra(QR_CODE_ID_KEY);
        nameField = findViewById(R.id.name_field_id);
        emailField = findViewById(R.id.email_field_id);
        phoneField = findViewById(R.id.phone_field_id);
        nickNameField = findViewById(R.id.nickName_field_id);
        abilityField = findViewById(R.id.ability_field_id);
        bornDateField = findViewById(R.id.born_date_field_id);
        hasPetSwitch = findViewById(R.id.has_pet_id);
        doneButton = findViewById(R.id.done_button_id);
        initProgressDialog();
        initDatePickerDialog();
        doneButton.setOnClickListener(v -> doneButtonClick());
        bornDateField.setOnClickListener(v -> datePickerDialog.show());

    }

    private void initDatePickerDialog() {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(this, AlertDialog.THEME_HOLO_DARK, (view, yearSelected, monthSelected, datSelected) -> {
            dateSelected = Calendar.getInstance();
            dateSelected.set(yearSelected, monthSelected, datSelected);
            bornDateField.setText(formatDateSelected(dateSelected,"dd/MM/yyyy"));
        }, year, month, day);
    }

    private String formatDateSelected(Calendar dateSelected,String outPutFormat){
        SimpleDateFormat sdf = new SimpleDateFormat(outPutFormat, Locale.US);
        return sdf.format(dateSelected.getTime());
    }

    private void initProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getText(R.string.creating_client_please_wait));
    }

    private void doneButtonClick() {
        if (checkEmptyRequiredField(nameField) || checkEmptyRequiredField(emailField)
                || checkEmptyRequiredField(nickNameField) || dateSelected == null
                || checkEmptyRequiredField(phoneField)) {
            Toasty.error(this, R.string.some_field_are_empties, Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (!IlParcoApplication.isNetworkAvailable(getApplicationContext())) {
            SnackbarHelper.showNoConnectionSnackBar(this, doneButton);
            return;
        }
        CreateClientRequestBody clientBodyRequest = new CreateClientRequestBody();
        clientBodyRequest.setName(getStringValue(nameField));
        clientBodyRequest.setEmail(getStringValue(emailField));
        clientBodyRequest.setNickname(getStringValue(nickNameField));
        clientBodyRequest.setBirthdate(formatDateSelected(dateSelected,"yyyy/MM/dd"));
        clientBodyRequest.setHas_pet(hasPetSwitch.isChecked() ? HAS_PET_TRUE : HAS_PET_FALSE);
        clientBodyRequest.setPhone(getStringValue(phoneField));
        clientBodyRequest.setSkill(getStringValue(abilityField));
        clientBodyRequest.setQr_id(qrCodeId);
        progressDialog.show();
        ilParcoRequestController.createClient(clientBodyRequest, client -> {
            progressDialog.dismiss();
            if (client == null) {
                Toasty.error(CreateClientActivity.this, R.string.error_creating_client, Toast.LENGTH_LONG, true).show();
            } else {
                Toasty.success(CreateClientActivity.this, R.string.client_creation_successful, Toast.LENGTH_SHORT, true).show();
                Intent intent = new Intent(CreateClientActivity.this, MainActivity.class);
                intent.putExtra(MainActivity.SUCCES_OPERATION,true);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    private String getStringValue(TextInputEditText textInputEditText) {
        Editable editable = textInputEditText.getText();
        if (editable == null) {
            return "";
        } else {
            String text = editable.toString();
            if (TextUtils.isEmpty(text)) {
                return "";
            } else {
                return text;
            }
        }
    }

    private boolean checkEmptyRequiredField(TextInputEditText field) {
        Editable text = field.getText();
        if (text != null) {
            return TextUtils.isEmpty(field.getText().toString());
        }
        return true;
    }
}
