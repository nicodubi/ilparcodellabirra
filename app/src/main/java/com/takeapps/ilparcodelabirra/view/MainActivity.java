package com.takeapps.ilparcodelabirra.view;

import android.Manifest;
import android.animation.Animator;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.takeapps.ilparcodelabirra.IlParcoApplication;
import com.takeapps.ilparcodelabirra.network_controller.IlParcoRequestController;
import com.takeapps.ilparcodelabirra.R;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import es.dmoral.toasty.Toasty;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;

public class MainActivity extends AppCompatActivity {

    public static final String SUCCES_OPERATION = "Sucess Operation";
    private TextView createClientButton;
    private TextView createPurchaseButton;
    public static final Collection<String> QR_CODE_FORMAT_SCAN =
            list(IntentIntegrator.QR_CODE);
    private int requestCodeToPerformAction;
    public static final int PURCHASE_TYPE = 1;
    public static final int NEW_CLIENT_TYPE = 2;
    private LottieAnimationView successAnimation;
    private Animator.AnimatorListener animatorListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {
            successAnimation.setVisibility(View.GONE);
        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    };

    @Inject
    public IlParcoRequestController ilParcoRequestController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Il Parco De La Birra");
        IlParcoApplication.getAppComponent().inject(this);
        createClientButton = findViewById(R.id.create_new_client_id);
        createPurchaseButton = findViewById(R.id.load_new_purchase_id);
        successAnimation = findViewById(R.id.success_animation);
        createClientButton.setOnClickListener(v -> {
            if (checkCameraPermissionGranted()) {
                requestCodeToPerformAction = NEW_CLIENT_TYPE;
                startScanActivity();
            }
        });

        createPurchaseButton.setOnClickListener(v -> {
            if (checkCameraPermissionGranted()) {
                requestCodeToPerformAction = PURCHASE_TYPE;
                startScanActivity();
            }
        });
    }

    private void startScanActivity() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setOrientationLocked(false);
        integrator.initiateScan(QR_CODE_FORMAT_SCAN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IntentIntegrator.REQUEST_CODE && resultCode == RESULT_OK) {
            IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (scanResult == null) {
                Toasty.error(this, R.string.error_scanning_qr_code, Toast.LENGTH_SHORT).show();
                return;
            }
            String qrCodeScanned = scanResult.getContents();
            if (this.requestCodeToPerformAction == NEW_CLIENT_TYPE) {
                goToCreateNewClient(qrCodeScanned);
            } else if (this.requestCodeToPerformAction == PURCHASE_TYPE) {
                goToCreateNewPurchase(qrCodeScanned);
            }
        }
    }

    private void goToCreateNewPurchase(String qrCodeId) {
        if (TextUtils.isEmpty(qrCodeId)) {
            Toasty.error(this, R.string.qr_readed_error, Toast.LENGTH_SHORT, false).show();
            return;
        }
        Intent intent = new Intent(this, CreatePurchaseActivity.class);
        intent.putExtra(CreatePurchaseActivity.QR_CODE_ID_KEY, qrCodeId);
        startActivity(intent);
    }

    private void goToCreateNewClient(String qrCodeId) {
        if (TextUtils.isEmpty(qrCodeId)) {
            Toasty.error(this, R.string.qr_readed_error, Toast.LENGTH_SHORT, false).show();
            return;
        }
        Intent intent = new Intent(this, CreateClientActivity.class);
        intent.putExtra(CreateClientActivity.QR_CODE_ID_KEY, qrCodeId);
        startActivity(intent);
    }

    private boolean checkCameraPermissionGranted() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if (Nammu.checkPermission(Manifest.permission.CAMERA)) {
            return true;
        }

        //We do not own this permission
        if (Nammu.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            //User already refused to give us this permission or removed it
            //Now he/she can mark "never ask again" (sic!)
            Snackbar.make(createClientButton, R.string.camera_permission_needed,
                    Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Nammu.askForPermission(MainActivity.this, Manifest.permission.CAMERA, new PermissionCallback() {
                        @Override
                        public void permissionGranted() {

                        }

                        @Override
                        public void permissionRefused() {

                        }
                    });
                }
            }).show();
        } else {
            //First time asking for permission
            // or phone doesn't offer permission
            // or user marked "never ask again"
            Nammu.askForPermission(this, Manifest.permission.CAMERA, new PermissionCallback() {
                @Override
                public void permissionGranted() {

                }

                @Override
                public void permissionRefused() {

                }
            });
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Nammu.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private static List<String> list(String... values) {
        return Collections.unmodifiableList(Arrays.asList(values));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getBooleanExtra(SUCCES_OPERATION, false)) {
            initSuccessAnimation("green_done_lottie.json");
        }
    }

    private void initSuccessAnimation(String filePath) {
        successAnimation.setVisibility(View.VISIBLE);
        successAnimation.setAnimation(filePath);
        successAnimation.addAnimatorListener(animatorListener);
        successAnimation.playAnimation();
    }
}

