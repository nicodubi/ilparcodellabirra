package com.takeapps.ilparcodelabirra.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.takeapps.ilparcodelabirra.R;

public class SplashActivity extends AppCompatActivity {

    private ImageView imageSplashLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        imageSplashLogo = findViewById(R.id.image_splash_id);
        animateLogo();
    }

    private void animateLogo() {

        ViewCompat.animate(imageSplashLogo)
                //  .translationY(TRANSALATION_VALUE_ANIM)
                .setStartDelay(500)
                .scaleX(2)
                .scaleY(2)
                .setDuration(1000).setInterpolator(
                new DecelerateInterpolator(1.2f)).setListener(new ViewPropertyAnimatorListener() {
            @Override
            public void onAnimationStart(View view) {

            }

            @Override
            public void onAnimationEnd(View view) {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }

            @Override
            public void onAnimationCancel(View view) {

            }
        }).start();
    }

}
